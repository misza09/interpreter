module Main where
import System.IO
import System.Environment
import qualified Data.Map as M
import Control.Monad.Except
import Control.Monad.Reader
import Control.Monad.State
import Data.List
import Data.Maybe
import Lexprog 
import Parprog
import Absprog
import ErrM
type Functions = M.Map String (Type, [(String, Type)],[Stm])
data Val = ValBool Bool| ValInt Integer | ValTuple [Val] deriving Eq
type Store = M.Map Integer Val
type Env =  M.Map String (Integer, Type)
type Memory = (Functions, Env, Store, Integer)
type Computation = StateT Memory IO

instance Show Val where
	show (ValInt a) = show a
	show (ValBool a) = show a
	show (ValTuple a) = show a

value :: String -> Computation Val
value name = do
	(funs, env, store, next) <- get
	(val, t) <- getEnvLoc name
	n <-gets (\(_, _, store, _) -> M.lookup val store)
	case n of
		Just x -> return x 
		otherwise -> error ("Undeclared value for "++name)


getNthElem :: [Val] -> Integer -> Computation Val
getNthElem [] _ = error ("Wrong index")
getNthElem (a:l) 0 = return a
getNthElem (a:l) i = getNthElem l (i-1) 


getEnvLoc :: String -> Computation(Integer, Type)
getEnvLoc name = do
	r <-gets (\(_, env, _, _) -> M.lookup name env)
	case r of
			Nothing -> error ("Undeclared variable " ++ name)
			Just (val,t) -> return (val,t)

isReturnDeclaredAndAssigned :: Computation Bool
isReturnDeclaredAndAssigned = do
	r <-gets (\(_, env, _, _) -> M.lookup "return" env)
	case r of
		Nothing -> return $ (False)
		Just (val,t) -> do 
			n <-gets (\(_, _, store, _) -> M.lookup val store)
			case n of
				Just x -> return $ (True)
				otherwise -> return $ (False)



prepareStore :: String -> Computation(Integer, Type)
prepareStore name = do
	(funs, env, store, next) <- get
	(val, t) <- getEnvLoc name
	case (M.lookup val store) of
			Nothing ->do
				initVal <-getInitVal t
				put (funs, env, M.insert val initVal store, next)
			otherwise -> return ()
				
	return (val,t)

getInitVal :: Type -> Computation Val
getInitVal TBool =  return $ (ValBool True)
getInitVal TInt =  return $ (ValInt 0)
getInitVal (TTuple []) =  return $ ValTuple []
getInitVal (TTuple (a:l)) = do
	(ValTuple t)<-getInitVal (TTuple l)
	h<-getInitVal a
  	return $ ValTuple (h:t)


checkTypes :: Type -> Val -> Computation()
checkTypes TInt (ValInt v) = return()
checkTypes TBool (ValBool v) = return()
checkTypes (TTuple []) (ValTuple []) = 
	return()
checkTypes (TTuple (t:ts)) (ValTuple (a:l)) = do
	checkTypes t a
	checkTypes (TTuple ts) (ValTuple l)	
checkTypes _ _ = error "Wrong type"



assign :: String -> Val -> Computation ()
assign name val = do
	(funs, env, store, next) <- get
	(no, t) <- prepareStore name
	checkTypes t val
	put (funs, env, M.insert no val store, next)
	return ()


getCellNo :: (String,(Integer, Type)) -> Integer
getCellNo (name,(no,t)) = no



getNextCell:: Computation Integer
getNextCell = do
	(funs, env, store, no) <- get
	put(funs, env, store, no+1)
	return $ (no+1)


declareVar :: String -> Type -> Computation ()
declareVar name t = do 
	cell <- getNextCell
	(funs, env, vars, no) <- get
	put (funs, M.insert name (cell,t) env, vars, no)
	return ()



bVALToBool :: BVAL -> Bool
bVALToBool (BVAL "true") = True
bVALToBool (BVAL "false") = False


getIntVal :: Val -> Integer 
getIntVal (ValInt a) = a
getIntVal (ValBool a) = error "Wrong type - expected type int, actual type bool"
getIntVal (ValTuple a) = error "Wrong type - expected type int, actual type tuple"

getBoolVal :: Val -> Bool 
getBoolVal (ValInt a) = error "Wrong type - expected type bool, actual type int"
getBoolVal (ValBool a) = a
getBoolVal (ValTuple a) = error "Wrong type - expected type bool, actual type tuple"


getTupleVal :: Val -> [Val] 
getTupleVal (ValInt a) = error "Wrong type - expected type tuple, actual type int"
getTupleVal (ValBool a) = error "Wrong type - expected type tuple, actual type bool"
getTupleVal (ValTuple a) = a



getValType :: Val -> Type
getValType (ValInt _) = TInt
getValType (ValBool _) = TBool
getValType (ValTuple _) = TTuple []

chechDiffZero :: Val -> Computation()
chechDiffZero v = do
	if (getIntVal v) == 0 then 
		error "Value 0 is not allowed"
	else 
		return ()


getVal :: AssVal -> Computation Val
getVal (AssValTup a) = getTupVal a
getVal (AssValExp a) = getExpVal a

getExpVal :: Exp -> Computation Val
getExpVal (Fun (Ident name) argsVal) = do
	(funs, env, _, _) <- get
	funcBody<-gets (\(funs, _, _, _) -> M.lookup name funs)
	case funcBody of
		Nothing -> error ("Function "++ name ++ " does not exist")
		Just (t, args, stms) -> do
			putArgs args argsVal
			declareVar "return" t 
			compute stms
			result <-value "return"
			(_, _, store2, next2) <- get
			put(funs, env, store2, next2)
			return result  where
				putArgs :: [(String, Type)] ->  [Exp] ->  Computation()
				putArgs [] [] = return ()
				putArgs ((name,t):tail) (exp:t2) = do
					v <-getExpVal exp
					declareVar name t
					assign name v
					putArgs tail t2
				putArgs _ _ = error "Wrong amount of arguments" 


getExpVal (ELt a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	return $ ValBool ((getIntVal x) < (getIntVal y))
getExpVal (EGt a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	return $ ValBool ((getIntVal x) > (getIntVal y))
getExpVal (ELte a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	return $ ValBool ((getIntVal x) <= (getIntVal y))
getExpVal (EGte a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	return $ ValBool ((getIntVal x) >= (getIntVal y))
getExpVal (EEq a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	return $ ValBool (x == y)
getExpVal (EDf a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	return $ ValBool (x /= y)
getExpVal (EBool val) = return $ ValBool (bVALToBool val)


getExpVal (EInt val) = return (ValInt val)
getExpVal (EMinus val) = do
		x <- getExpVal val 
		return $ ValInt (-(getIntVal x))
getExpVal (EAdd a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	return $ ValInt ((getIntVal x) + (getIntVal y))
getExpVal (ESub a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	return $ ValInt ((getIntVal x) - (getIntVal y))
getExpVal (EMul a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	return $ ValInt ((getIntVal x) * (getIntVal y))
getExpVal (EDiv a b) = do
	x <- getExpVal a 
	y <- getExpVal b
	chechDiffZero y
	return $ ValInt ((getIntVal x) `div` (getIntVal y))

getExpVal (EStm(EInc (Ident name))) = do
	v <-value name
	assign name (ValInt ((getIntVal v) + 1))
	return v
getExpVal (EStm(EDec (Ident name))) = do
	v <-value name
	assign name (ValInt ((getIntVal v) - 1))
	return v
getExpVal (EVar (Ident name)) = do
	v <-value name
	return v

getExpVal (ETupVar exp idx) = do
	val <- getExpVal exp
	ret <- getNthElem (getTupleVal val) idx
	return $ ret

getTupVal :: [AssVal] -> Computation Val
getTupVal [] = return $ (ValTuple [])
getTupVal (a:l) = do
	 x <- getVal a
	 y <- getTupVal l
	 return $ ValTuple (x:(unpackTuple y))
	
unpackTuple :: Val -> [Val]
unpackTuple (ValTuple a) = a



checkVarProperName :: String -> Computation ()
checkVarProperName name =
	if name == "return" then 
		error "Cannot assign variable 'return'. Use return exp instead "
	else
		return ()


computeSCStm :: CStm -> Computation ()
computeSCStm (SAdd (Ident name) exp) = do
	checkVarProperName name
	v <-value name
	v2 <- getExpVal exp
	assign name (ValInt ((getIntVal v) + (getIntVal v2)))
computeSCStm (SSub (Ident name) exp) = do
	checkVarProperName name
	v <-value name
	v2 <- getExpVal exp
	assign name (ValInt ((getIntVal v) - (getIntVal v2)))
computeSCStm (SMul (Ident name) exp) = do
	checkVarProperName name
	v <-value name
	v2 <- getExpVal exp
	assign name (ValInt ((getIntVal v) * (getIntVal v2)))
computeSCStm (SDiv(Ident name) exp) = do
	checkVarProperName name
	v <-value name
	v2 <- getExpVal exp
	chechDiffZero v2
	assign name (ValInt ((getIntVal v) `div` (getIntVal v2)))
computeSCStm (SAss [] []) = do
	return()
computeSCStm (SAss((Ident name):names) (exp:exps)) = do
	checkVarProperName name
	v <- getVal exp
	assign name v
	computeSCStm (SAss names exps)
computeSCStm (SAss _ _ ) =
	error "Wrong amount of values in assignment" 



		
setStmType :: ChVal -> Stm
setStmType (SChValExp exp) = SExp  (EStm exp )
setStmType (SChValStm stm) = SCStm stm


parseParams :: [Arg] -> [(String, Type)]
parseParams l = foldr f [] l where
		f (FunArg t (Ident name)) ret =  ((name,t):ret)
	

compute :: [Stm] ->Computation()
compute[] = return ()
compute (a:l) = do
	ret <- isReturnDeclaredAndAssigned
	if ret then
		return ()
	else
		(go a >> compute l)
	where
	go (SDecl decl) = do
		declare decl  where
			declare (Dec t (Ident name)) = declareVar name t
			declare (DecAss t (Ident name) exp) = do
				v <-getVal exp
				declareVar name t
			 	assign name v

				
			


	go (SWrite val) = getVal(val) >>= liftIO . print	
	go (SCStm stm) = computeSCStm stm
	go (SExp expr) = getExpVal(expr) >> return () 
	go (SWhile bexpr []) = return ()
	go l@(SWhile exp stmts) = do
		(funs, env, store, next) <- get
		cond <- getExpVal exp
		if getBoolVal cond then do
			compute stmts
			(funs2, env2, store2, next2) <- get
			put(funs, env, store2, next2) 
			go l
		else
			return ()				

	 		
	go (SFor2 decl exp stmts) = do
		(funs, env, store, next) <- get
		go (SDecl decl)
		go (SWhile exp stmts)
		(funs2, env2, store2, next2) <- get
		put(funs, env, store2, next2)

	go (SFor3 decl exp stm stmts) = do
		(funs, env, store, next) <- get
		go (SDecl decl)
		go (SWhile exp (stmts ++ [setStmType stm]))
		(funs2, env2, store2, next2) <- get
		put(funs, env, store2, next2)
		return ()
	go (SIf exp stmts elifs) = do
		(funs, env, store, next) <- get
		cond <- getExpVal exp
		if getBoolVal cond then
			compute stmts
		else
			computeElif elifs []
		(funs2, env2, store2, next2) <- get
		put(funs, env, store2, next2)
	go (SIfElse exp stmts elifs elsestm) = do
		(funs, env, store, next) <- get
		cond <- getExpVal exp
		if getBoolVal cond then
			compute stmts
		else
			computeElif elifs elsestm
		(funs2, env2, store2, next2) <- get
		put(funs, env, store2, next2)

	go (SReturn exp) = do
		v <-getExpVal exp
		assign "return" v

	go (FunDecl t (Ident name) params stmts) = do
		(funs, env, store, next) <- get
		put (M.insert name (t, parseParams(params), stmts) funs, env, store, next)
		return ()


computeElif :: [Elif] -> [Stm] -> Computation()		
computeElif [] elsestm = do
	compute elsestm
computeElif ((IfElif exp stmts):t) elsestm = do
	cond <- getExpVal exp
	if getBoolVal cond then
		compute stmts
	else
		computeElif t elsestm

	
runProgram :: String -> IO()
runProgram s=
	case pProgram $ myLexer s of
	Bad a -> putStrLn a
	Ok (Prog t) ->
		evalStateT (compute t) (M.empty, M.empty, M.empty, 0)
		
main :: IO()
main = do
	name <- getProgName
	args <- getArgs
	case args of
		[fp] -> readFile fp >>= runProgram
		_ -> getContents >>= runProgram




