all:interpreter

interpreter: Interpreter.hs deps
	ghc --make Interpreter.hs -o interpreter

deps:
	happy -gca Parprog.y
	alex -g Lexprog.x
	latex Docprog.tex; dvips Docprog.dvi -o Docprog.ps
	ghc --make Testprog.hs -o Testprog
clean:
	-rm -f *.log *.aux *.hi *.o *.dvi
	-rm -f Docprog.ps
	-rm -f interpreter
	-rm -f Testprog
distclean: clean
	-rm -f Docprog.* Lexprog.* Parprog.* Layoutprog.* Skelprog.* Printprog.* Testprog.* Absprog.* Testprog ErrM.* SharedString.* prog.dtd XMLprog.* 

